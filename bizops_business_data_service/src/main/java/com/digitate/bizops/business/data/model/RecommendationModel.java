package com.digitate.bizops.business.data.model;

import org.springframework.data.annotation.Id;

public class RecommendationModel {
	@Id
	String id;
	String user_id;
	String model_name;
	String mapped_data;
	String key;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getModel_name() {
		return model_name;
	}
	public void setModel_name(String model_name) {
		this.model_name = model_name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getMapped_data() {
		return mapped_data;
	}
	public void setMapped_data(String mapped_data) {
		this.mapped_data = mapped_data;
	}
	
	

}
