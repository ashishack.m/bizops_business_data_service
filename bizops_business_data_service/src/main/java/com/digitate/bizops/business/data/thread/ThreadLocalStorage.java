package com.digitate.bizops.business.data.thread;

public class ThreadLocalStorage {
	private static ThreadLocal<String> tenant = new ThreadLocal<>();
	private static ThreadLocal<String> user = new ThreadLocal<>();
	
	

    public static void setUserName(String userName) {
    	
    	user.set(userName);
    }

    public static String getUserName() {
        return user.get();
    }
	
	
	
    public static void setTenantName(String tenantName) {
    	if(tenantName==null) {
    		tenantName="GlobalDataSource";
    	}
        tenant.set(tenantName);
    }

    public static String getTenantName() {
        return tenant.get();
    }
}
