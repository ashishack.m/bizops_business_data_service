package com.digitate.bizops.business.data.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "business_data_ingestion_history")
public class RequestHistory {
	@Id
	private String referenceID;

	private String start_timestamp;

	private String end_timestamp;

	private List<StatusModel> status;

	private String user;

	private String result;

	private String remark;
	private long seq;
	private String status_message;
	private String ingestion_id;
	private long success_count;
	private long error_count;
	
	



	public long getSeq() {
		return seq;
	}

	public void setSeq(long seq) {
		this.seq = seq;
	}

	public long getSuccess_count() {
		return success_count;
	}

	public void setSuccess_count(long success_count) {
		this.success_count = success_count;
	}

	public long getError_count() {
		return error_count;
	}

	public void setError_count(long error_count) {
		this.error_count = error_count;
	}

	public String getIngestion_id() {
		return ingestion_id;
	}

	public void setIngestion_id(String ingestion_id) {
		this.ingestion_id = ingestion_id;
	}

	public String getReferenceID() {
		return referenceID;
	}

	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}

	public String getStart_timestamp() {
		return start_timestamp;
	}

	public void setStart_timestamp(String start_timestamp) {
		this.start_timestamp = start_timestamp;
	}

	public String getEnd_timestamp() {
		return end_timestamp;
	}

	public void setEnd_timestamp(String end_timestamp) {
		this.end_timestamp = end_timestamp;
	}

	public List<StatusModel> getStatus() {
		return status;
	}

	public void setStatus(List<StatusModel> status) {
		this.status = status;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus_message() {
		return status_message;
	}

	public void setStatus_message(String status_message) {
		this.status_message = status_message;
	}
	
}
