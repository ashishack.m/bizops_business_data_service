package com.digitate.bizops.business.data.model;



public class RequestHistoryUiModel {
	
	String reference_id;
	
	private String start_date;

	private String end_date;


	private String ingested_by;


	private String status_message;
	private long  success_count;
	private long error_count;
	String remark;
	
	
	public long getSuccess_count() {
		return success_count;
	}
	public void setSuccess_count(long success_count) {
		this.success_count = success_count;
	}
	public long getError_count() {
		return error_count;
	}
	public void setError_count(long error_count) {
		this.error_count = error_count;
	}
	public String getReference_id() {
		return reference_id;
	}
	public void setReference_id(String reference_id) {
		this.reference_id = reference_id;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getIngested_by() {
		return ingested_by;
	}
	public void setIngested_by(String ingested_by) {
		this.ingested_by = ingested_by;
	}

	public String getStatus_message() {
		return status_message;
	}
	public void setStatus_message(String status_message) {
		this.status_message = status_message;
	}


	
}
