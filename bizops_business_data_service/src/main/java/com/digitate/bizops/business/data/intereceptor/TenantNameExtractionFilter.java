package com.digitate.bizops.business.data.intereceptor;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.digitate.bizops.business.data.thread.ThreadLocalStorage;

@Component
@Order(-5000)
public class TenantNameExtractionFilter implements Filter {
    private Logger logger = LoggerFactory.getLogger(TenantNameExtractionFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;

        String tenant = null;
        System.out.println(request.getRequestURI());
        //if ("/enterprise_data/ingestion _history".equalsIgnoreCase(request.getRequestURI())) {
            try {
                tenant = request.getParameter("tenant");
            } catch (Exception e) {
                tenant = null;
            }
        //}


        try {
            ThreadLocalStorage.setTenantName(tenant);
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            ThreadLocalStorage.setTenantName("global");
        }

    }

//    private Map < String, ? > parseToken(String token) {
//        JsonParser parser = JsonParserFactory.getJsonParser();
//        return parser.parseMap(JwtHelper.decode(token).getClaims());
//   }

}
