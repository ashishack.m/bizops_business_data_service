package com.digitate.bizops.business.data.model;

import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.data.annotation.Id;

public class IngestionModel {

	@Id
	private String id;
	private String entity_name;
	private JSONObject entity_info;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEntity_name() {
		return entity_name;
	}
	public void setEntity_name(String entity_name) {
		this.entity_name = entity_name;
	}
	public JSONObject getEntity_info() {
		return entity_info;
	}
	public void setEntity_info(JSONObject entity_info) {
		this.entity_info = entity_info;
	}

	
}
