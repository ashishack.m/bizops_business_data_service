package com.digitate.bizops.business.data.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.digitate.bizops.business.data.model.TenantInfo;

@Repository
@Transactional
public interface TenantInfoRepository extends CrudRepository < TenantInfo, String > {

	List<TenantInfo> findAll();
	TenantInfo findByTenantName(String tenantName);
	TenantInfo findByTenantNameAndService(String tenantName,String service);
}
