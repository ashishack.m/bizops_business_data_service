package com.digitate.bizops.business.data.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.digitate.bizops.business.data.model.DatabaseServerInfo;

@Repository
@Transactional
public interface DatabaseServerInfoRepository  extends CrudRepository < DatabaseServerInfo, String >{

	DatabaseServerInfo findById(int id);
	
}