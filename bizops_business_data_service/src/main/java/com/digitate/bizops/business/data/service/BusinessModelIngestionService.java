package com.digitate.bizops.business.data.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.digitate.bizops.business.data.model.DatabaseConfiguration;
import com.digitate.bizops.business.data.model.DatabaseServerInfo;
import com.digitate.bizops.business.data.model.InProgressModel;
import com.digitate.bizops.business.data.model.IngestionModel;
import com.digitate.bizops.business.data.model.RecommendationModel;
import com.digitate.bizops.business.data.model.RequestHistory;
import com.digitate.bizops.business.data.model.RequestHistoryUiModel;
import com.digitate.bizops.business.data.model.RequestSequence;
import com.digitate.bizops.business.data.model.StatusModel;
import com.digitate.bizops.business.data.model.TenantInfo;
import com.digitate.bizops.business.data.thread.ThreadLocalStorage;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.connection.ConnectionPoolSettings;
import com.mongodb.connection.SslSettings;

@Service
public class BusinessModelIngestionService {
	
	@Value("${ingestion.steps}")
	private String ingestionSteps;
	@Value("${spring.application.name}")
	private String appName;
	@Autowired
	GlobalDbServiceImpl globalDbServiceImpl;
	@Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;
	
	private String getMongoClientSettings(int connectionPoolMaxSize,String host,String classPath,String userName,String password) {
        if(userName==null||"".equalsIgnoreCase(userName))
        	return classPath+host+"?maxPoolSize="+connectionPoolMaxSize;
        else
        	return classPath+userName+":"+password+"@"+host+"?maxPoolSize="+connectionPoolMaxSize;
    }
	
	public List<InProgressModel>  getInProgressInfo(String user,int limit){
		List<InProgressModel> result =null;
		try {
			result =new ArrayList<InProgressModel>();
			
			DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
			MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
			MongoTemplate mongotemp = new MongoTemplate(client, config.getDbName());
			
			Query query = new Query(Criteria.where("user").is(user)); //.andOperator(Criteria.where("status_message").is("Inprogress")));
			query.with(Sort.by(Sort.Direction.DESC, "seq")).limit(limit);
			
			List<RequestHistory> inProgressList=mongotemp.find(query, RequestHistory.class, "business_data_ingestion_history");
			
				for(RequestHistory reqHistory:inProgressList) {
					
					InProgressModel inprogress=new InProgressModel();
					List<StatusModel> statusList=reqHistory.getStatus();
					StatusModel status =null;
					String flag = "";
					
					for(StatusModel tempStatus:statusList) {
						flag = "";
						if("error".equalsIgnoreCase(tempStatus.getStatus())) {
							status =tempStatus;
							flag ="error";
							break;
						}
						if("Not started".equalsIgnoreCase(tempStatus.getStatus()) || "started".equalsIgnoreCase(tempStatus.getStatus())) {
							status =tempStatus;
							flag ="inProgress";
							break;
						}
						if(tempStatus.getSeqNumber()==statusList.size() && tempStatus.getStatus().equalsIgnoreCase("completed")) {
							status =tempStatus;
							flag ="completed";
							break;
						}
					}
					inprogress.setEndDateTime("");
					inprogress.setStartDateTime(reqHistory.getStart_timestamp());
					inprogress.setTitle("Reference ID: "+reqHistory.getReferenceID());
					inprogress.setRemark(reqHistory.getRemark());
					switch (flag) {
						case "inProgress" :
							if(status==null) {
								inprogress.setStepName(null);
								inprogress.setPercent(0);
								inprogress.setStepsCompleted("0 of "+statusList.size());
							}
							else {
								inprogress.setStepName(status.getStepName());
								if(status.getSeqNumber()!=1) {
									double percent=(((double)(status.getSeqNumber()-1)/(double)statusList.size())*100);
									inprogress.setPercent((int) percent);
									inprogress.setStepsCompleted((status.getSeqNumber()-1)+" of "+statusList.size());
								}else {
									inprogress.setPercent((1/statusList.size())*100);
									inprogress.setStepsCompleted("0 of "+statusList.size());
								}
							}
							result.add(inprogress);
							break;
						case "completed" :
							inprogress.setEndDateTime(reqHistory.getEnd_timestamp());
							inprogress.setPercent(100);
							inprogress.setStepName(status.getStepName());
							inprogress.setStepsCompleted((status.getSeqNumber())+" of "+statusList.size());
							inprogress.setTitle("Reference ID: "+reqHistory.getReferenceID());
							result.add(inprogress);
							break;
						case "error" :
							inprogress.setEndDateTime(reqHistory.getEnd_timestamp());
							inprogress.setPercent(0);
							inprogress.setStepName(status.getStepName()+" - ERROR ");
							inprogress.setStepsCompleted((status.getSeqNumber())+" of "+statusList.size());
							inprogress.setTitle("Reference ID: "+reqHistory.getReferenceID());
							result.add(inprogress);	
							break;
					}
				}
			client.close();
		}catch(Exception e) {
			throw e;
		}
		
		return result;
	}
	
	public List<InProgressModel>  getInProgressInfo_old(String user,int limit){
		List<InProgressModel> result =null;
		try {
			result =new ArrayList<InProgressModel>();
			DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
			MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
			MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
			
			Query query = new Query(Criteria.where("user").is(user).andOperator(Criteria.where("status_message").is("Inprogress")));
			query.with(Sort.by(Sort.Direction.DESC, "seq")).limit(limit);
			
			List<RequestHistory> inProgressList=mongoOps.find(query, RequestHistory.class, "business_data_ingestion_history");
			if(inProgressList.size()<limit) {
				
				query = new Query(Criteria.where("user").is(user).andOperator(Criteria.where("status_message").is("completed")));
				query.with(Sort.by(Sort.Direction.DESC, "seq")).limit(limit);
				List<RequestHistory> completedList=mongoOps.find(query, RequestHistory.class, "business_data_ingestion_history");
				int count=0;
				for(RequestHistory reqHistory:inProgressList) {
					
					InProgressModel inprogress=new InProgressModel();
					List<StatusModel> statusList=reqHistory.getStatus();
					StatusModel status =null;
					
					for(StatusModel tempStatus:statusList) {
						//System.out.println(reqHistory.getReferenceID()+"  status="+tempStatus.getStatus());
						if("Not started".equalsIgnoreCase(tempStatus.getStatus())) {
							status =tempStatus;
							break;
						}
					}
					inprogress.setEndDateTime("");
					inprogress.setStartDateTime(reqHistory.getStart_timestamp());
					inprogress.setTitle("Reference ID: "+reqHistory.getReferenceID());
					if(status==null) {
						inprogress.setStepName(null);
						inprogress.setPercent(0);
						inprogress.setStepsCompleted("0 of "+statusList.size());
					}
					else {
						inprogress.setStepName(status.getStepName());
						if(status.getSeqNumber()!=1) {
							double percent=(((double)(status.getSeqNumber()-1)/(double)statusList.size())*100);
							inprogress.setPercent((int) percent);
							inprogress.setStepsCompleted((status.getSeqNumber()-1)+" of "+statusList.size());
						}else {
							inprogress.setPercent((1/statusList.size())*100);
							inprogress.setStepsCompleted("0 of "+statusList.size());
						}
					}
					
					result.add(inprogress);
					count++;
				}
				for(RequestHistory reqHistory:completedList) {
					if(count==limit) {
						break;
					}else {
						InProgressModel completed=new InProgressModel();
						List<StatusModel> statusList=reqHistory.getStatus();
						StatusModel status =new StatusModel();
						
						for(StatusModel tempStatus:statusList) {
							if(tempStatus.getSeqNumber()==statusList.size()) {
								status =tempStatus;
								break;
							}
						}
						completed.setEndDateTime(reqHistory.getEnd_timestamp());
						completed.setPercent(100);
						completed.setStartDateTime(reqHistory.getStart_timestamp());
						completed.setStepName(status.getStepName());
						completed.setStepsCompleted((status.getSeqNumber())+" of "+statusList.size());
						completed.setTitle("Reference ID: "+reqHistory.getReferenceID());
						
						result.add(completed);
					}
					count++;
				}
				
			}
			else {
				for(RequestHistory reqHistory:inProgressList) {
					
					InProgressModel inprogress=new InProgressModel();
					List<StatusModel> statusList=reqHistory.getStatus();
					StatusModel status =null;
					
					for(StatusModel tempStatus:statusList) {
						if("Not started".equalsIgnoreCase(tempStatus.getStatus())) {
							status =tempStatus;
							break;
						}
					}
					inprogress.setEndDateTime("");
					inprogress.setStartDateTime(reqHistory.getStart_timestamp());
					inprogress.setTitle("Reference ID: "+reqHistory.getReferenceID());
					if(status==null) {
						inprogress.setStepName(null);
						inprogress.setPercent(0);
						inprogress.setStepsCompleted("0 of "+statusList.size());
					}
					else {
						inprogress.setStepName(status.getStepName());
						if(status.getSeqNumber()!=1) {
							double percent=(((double)(status.getSeqNumber()-1)/(double)statusList.size())*100);
							inprogress.setPercent((int) percent);
							inprogress.setStepsCompleted((status.getSeqNumber()-1)+" of "+statusList.size());
						}else {
							inprogress.setPercent((1/statusList.size())*100);
							inprogress.setStepsCompleted("0 of "+statusList.size());
						}
					}
					result.add(inprogress);
				}
			}
			client.close();
		}catch(Exception e) {
			throw e;
		}
		
		return result;
	}

	public HashMap<String, Object> getHistory(String user, int pageNo, int pageSize){
		List<RequestHistoryUiModel> history=null;
		HashMap<String, Object> historyResult = new HashMap<String, Object>();
		long count =0;
		try{
			
			Pageable paging = PageRequest.of(pageNo, pageSize,Sort.by(Sort.Direction.DESC, "seq"));
			DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
			MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
			MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
			
			history=new ArrayList<RequestHistoryUiModel>();

			////added to exlcude top 5 analysis
			Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(Criteria.where("user").is(user)),
					Aggregation.sort(Sort.Direction.DESC, "seq"),
                    Aggregation.skip(paging.getPageNumber() * paging.getPageSize()+5),
                    Aggregation.limit(paging.getPageSize()));
			AggregationResults<RequestHistory> ingestionHistoryex5 = mongoOps.aggregate(aggregation, RequestHistory.class, RequestHistory.class );
			//List<RequestHistory> BottomEntityInference = BottomEntityInferenceRaw.getMappedResults();
			////
			
//			List<RequestHistory> historyList=mongoOps.find(query, RequestHistory.class, "business_data_ingestion_history");
			for(RequestHistory requestHistory:ingestionHistoryex5) {
				RequestHistoryUiModel uiHistory=new RequestHistoryUiModel();
				uiHistory.setEnd_date(requestHistory.getEnd_timestamp());
				uiHistory.setStart_date(requestHistory.getStart_timestamp());
				uiHistory.setError_count(requestHistory.getError_count());
				uiHistory.setIngested_by(requestHistory.getUser());
				uiHistory.setReference_id(requestHistory.getReferenceID());
				uiHistory.setRemark(requestHistory.getRemark());
				uiHistory.setSuccess_count(requestHistory.getSuccess_count());
				uiHistory.setStatus_message(requestHistory.getStatus_message());
				history.add(uiHistory);
			}
			Query query = new Query(Criteria.where("user").is(user));
			query.with(paging);
			query = new Query(Criteria.where("user").is(user));
			count=mongoOps.count(query, "business_data_ingestion_history")-5;
			if (count == 0) {
				count=1;
			}else {
				count=count % pageSize != 0 ? count / pageSize + 1 : count / pageSize;
			}
			historyResult.put("data", history);
			historyResult.put("page_no", pageNo+1);
			historyResult.put("page_size", pageSize);
			historyResult.put("total_pages",count);
			
			client.close();
		}catch(Exception e) {
			throw e;
		}
		
		return historyResult;
	}
	public void saveReferenceInfo(RequestHistory requestHistory,String tenantName) {
		try{
			DatabaseConfiguration config=getDatabaseConfigurations(tenantName);
			MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
			MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
			//Query query = new Query(Criteria.where("_id").is(requestHistory.getReferenceID()));
			mongoOps.save(requestHistory, "business_data_ingestion_history");
			client.close();
		}catch(Exception e) {
			throw e;
		}
	}
	public RequestHistory getRefrenceInfo(String referenceID){
		RequestHistory history=null;
		try{
			DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
			MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
			MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
			
			history=new RequestHistory();
			Query query = new Query(Criteria.where("_id").is(referenceID));
			history=mongoOps.findOne(query, RequestHistory.class, "business_data_ingestion_history");
			client.close();
		}catch(Exception e) {
			throw e;
		}
		
		return history;
	}
	public RequestHistory getRefrenceInfoByTenant(String referenceID,String tenantName){
		RequestHistory history=null;
		try{
			DatabaseConfiguration config=getDatabaseConfigurations(tenantName);
			MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
			MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
			
			history=new RequestHistory();
			Query query = new Query(Criteria.where("_id").is(referenceID));
			history=mongoOps.findOne(query, RequestHistory.class, "business_data_ingestion_history");
			client.close();
		}catch(Exception e) {
			throw e;
		}
		
		return history;
	}
	public List<StatusModel> getStatus(String referenceId){
		List<StatusModel>status =null;
		try {
			DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
			MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
			MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
			
			Query query = new Query(Criteria.where("_id").is(referenceId));
			RequestHistory reqHistory=mongoOps.findOne(query, RequestHistory.class, "business_data_ingestion_history");
			status=reqHistory.getStatus();
			client.close();
		}catch(Exception e) {
			throw e;
		}
		return status;
	}
	public String[] getDataHeaders(String filePath) throws IOException{
		String[] columnNames=null;
		BufferedReader br = new BufferedReader(new FileReader(filePath));
        String header = br.readLine();
        if (header != null) {
        	columnNames = header.split(",");
        }
		return columnNames;
	}
	
	public IngestionModel getModel(String modelName){
		 
		IngestionModel model=null;
		try {
			DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
			MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
			MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
			
		 Query query = new Query(Criteria.where("entity_name").is(modelName));
		 model=mongoOps.findOne(query, IngestionModel.class, "entity_model");
		 client.close();
		}catch(Exception e) {
			throw e;
		}
		 return model;
	}
	
	public List<String> getModelMapList(String modelName,String key){
		 
		List<String> mapData=null;
		try {
			DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
			MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
			MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
			
		 Query query = new Query(Criteria.where("entity_name").is(modelName));
		 IngestionModel model=mongoOps.findOne(query, IngestionModel.class, "entity_model");
		 mapData=(List<String>) model.getEntity_info().get(key);
		 client.close();
		}catch(Exception e) {
			throw e;
		}
		 return mapData;
	}
	
	
	public long generateSequence(String seqName) {
		DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
		
		
		MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
		MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
		
		Query query = new Query(Criteria.where("_id").is(seqName));

		Update update = new Update();
		update.inc("seq", 1);

		FindAndModifyOptions options = new FindAndModifyOptions();
		options.returnNew(true);

		RequestSequence seqId =mongoOps.findAndModify(query, update, options, RequestSequence.class);
		
		client.close();
		return seqId.getSeq();
	}
	
	public boolean CreateNewRequest(String user,String remark,String referenceId,String ingestionId,long rowCount,long seq)
	{
		DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
		MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
		MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
		
		RequestHistory reqHistory = new RequestHistory();
		
		reqHistory.setReferenceID(referenceId);
		reqHistory.setUser(user);
		reqHistory.setRemark(remark);
		Date start_date = new Date();
	      SimpleDateFormat ft =  new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		reqHistory.setStart_timestamp(ft.format(start_date));
		String [] ingestionStepsArr = ingestionSteps.split(",");
		List<StatusModel> statusList = new ArrayList<StatusModel>();
		for(int i=0;i<ingestionStepsArr.length;i++)
		{
			StatusModel statusModel = new StatusModel();
			statusModel.setStepName(ingestionStepsArr[i]);
			statusModel.setStatus("Not started");
			statusModel.setSeqNumber(i+1);
			statusModel.setEnd_timestamp(null);
			statusList.add(statusModel);
			
		}
		reqHistory.setError_count(0);
		reqHistory.setSuccess_count(rowCount);
		reqHistory.setStatus(statusList);
		reqHistory.setResult(null);
		reqHistory.setEnd_timestamp(null);
		reqHistory.setStatus_message("In-progress");
		reqHistory.setIngestion_id(ingestionId);
		reqHistory.setSeq(seq);
		//requestRepository.save(reqHistory);
		
		mongoOps.save(reqHistory, "business_data_ingestion_history");
		
		client.close();
		return true;
	}
	public Map<String,String> getRecommendation(String userId, String[] headers,String modelName) {
		Map<String,String> recommendationMap=new HashMap<String,String>();
		DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
		
		MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
		MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
		
		for(String header:headers) {
			String cleanedHeader=header.replaceAll("[^A-Za-z0-9]","").toLowerCase().trim();
			Query query = new Query(Criteria.where("user_id").is(userId).andOperator(Criteria.where("key").is(cleanedHeader).andOperator(Criteria.where("model_name").is(modelName))));
			if(mongoOps.exists(query, RecommendationModel.class, "enterprise_recommendation")) {
				recommendationMap.put(header, mongoOps.findOne(query, RecommendationModel.class, "enterprise_recommendation").getMapped_data());
			}else {
				query = new Query(Criteria.where("user_id").is("default").andOperator(Criteria.where("key").is(cleanedHeader).andOperator(Criteria.where("model_name").is(modelName))));
				if(mongoOps.exists(query, RecommendationModel.class, "enterprise_recommendation"))
					recommendationMap.put(header, mongoOps.findOne(query, RecommendationModel.class, "enterprise_recommendation").getMapped_data());
				else
					recommendationMap.put(header, null);
			}
		}
		client.close();
		return recommendationMap;
		
	}
	public void saveRecommendation(String userId, String dataMap, String modelName) throws Exception {
		JSONParser parser = new JSONParser();
		try {
			DatabaseConfiguration config=getDatabaseConfigurations(ThreadLocalStorage.getTenantName());
			MongoClient client=MongoClients.create(getMongoClientSettings(config.getMaxPoolSize(),config.getHost(),config.getDataSourceClassName(),config.getUser(),config.getPassword()));
			MongoOperations mongoOps = new MongoTemplate(client, config.getDbName());
			
			JSONObject jObject=(JSONObject) parser.parse(dataMap);
			List<String> keys=new ArrayList<String>(jObject.keySet());
			for(String key:keys) {
				String cleanedHeader=key.replaceAll("[^A-Za-z0-9]","").toLowerCase().trim();
				Query query = new Query(Criteria.where("user_id").is(userId).andOperator(Criteria.where("key").is(cleanedHeader).andOperator(Criteria.where("model_name").is(modelName))));
				if(mongoOps.exists(query, RecommendationModel.class, "enterprise_recommendation")) {
					RecommendationModel model=mongoOps.findOne(query, RecommendationModel.class, "enterprise_recommendation");
					model.setMapped_data((String) jObject.get(key));
					mongoOps.save(model, "enterprise_recommendation");
				}else {
					RecommendationModel model=new RecommendationModel();
					model.setMapped_data((String) jObject.get(key));
					model.setUser_id(userId);
					model.setModel_name(modelName);
					model.setKey(cleanedHeader);
					mongoOps.save(model, "enterprise_recommendation");
				}
			}
			client.close();
		} catch (Exception e) {
			throw e;
		}
		
	}

	private DatabaseConfiguration getDatabaseConfigurations(String tenant) {
        DatabaseConfiguration databaseConfig = new DatabaseConfiguration();

            TenantInfo tenantInfo = globalDbServiceImpl.getTenantInfoByService(tenant, "ingestion-model-service");
            if (tenantInfo != null) {
                DatabaseServerInfo serverInfo = globalDbServiceImpl.getDataBaseInfoById(tenantInfo.getDbServerId());
                databaseConfig.setDataSourceClassName(serverInfo.getClassName());
                databaseConfig.setTenant(tenant);
                databaseConfig.setDbName(tenantInfo.getTenantDbName());
                databaseConfig.setHost(serverInfo.getIpAddress() );
                databaseConfig.setUser(serverInfo.getUserName());
                databaseConfig.setPassword(serverInfo.getPassword());
                databaseConfig.setMaxPoolSize(serverInfo.getMaxPoolSize());
                databaseConfig.setMinIdleConnection(serverInfo.getMinIdleConnection());
            } 

        return databaseConfig;
    }
}
