package com.digitate.bizops.business.data.status;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Service;

import com.digitate.bizops.business.data.model.RequestHistory;
import com.digitate.bizops.business.data.model.StatusInfo;
import com.digitate.bizops.business.data.model.StatusModel;
import com.digitate.bizops.business.data.service.BusinessModelIngestionService;
import com.digitate.bizops.business.data.thread.ThreadLocalStorage;

@Service
@EnableBinding(Sink.class)
public class EnterpiseModelServiceStatus {
	Logger logger = LoggerFactory.getLogger(EnterpiseModelServiceStatus.class);
	@Autowired
	BusinessModelIngestionService businessModelIngestionService;
	@Value("${ingestion.steps}")
	private String ingestionSteps;
	@Value("${baseDir.path}")
	private String baseDirectory;
	@Value("${path.seperator}")
	private String pathSeperator;
	@Value("${default.input.file.name}")
	private String defaultInputFileName;
	
	
	@StreamListener(target = Sink.INPUT)
	public void processEnterpriseModelStatus(StatusInfo statusInfo){
		System.out.println(statusInfo.getStepName() +" status = "+statusInfo.getStatus()+" tenant = "+statusInfo.getTenantName());
		RequestHistory referenceInfo=businessModelIngestionService.getRefrenceInfoByTenant(statusInfo.getReferenceId(),statusInfo.getTenantName());
		String [] ingestionStepsArr = ingestionSteps.split(",");
		List<StatusModel> statusList=referenceInfo.getStatus();
		List<StatusModel> updateStatusList=new ArrayList<StatusModel>();
		
		if("error".equalsIgnoreCase(statusInfo.getStatus())) {
			for(StatusModel status:statusList) {
				StatusModel updateStatus=new StatusModel();
				if(status.getSeqNumber()==statusInfo.getSeqNumber()) {
					updateStatus.setStepName(statusInfo.getStepName());
					updateStatus.setStatus(statusInfo.getStatus());
					updateStatus.setSeqNumber(statusInfo.getSeqNumber());
					updateStatus.setEnd_timestamp(statusInfo.getEnd_timestamp());
					updateStatusList.add(updateStatus);
					}
				else {
					updateStatus.setStepName(status.getStepName());
					updateStatus.setStatus(status.getStatus());
					updateStatus.setSeqNumber(status.getSeqNumber());
					updateStatus.setEnd_timestamp(status.getEnd_timestamp());
					updateStatusList.add(updateStatus);
					}
			}
			referenceInfo.setError_count(referenceInfo.getSuccess_count());
			referenceInfo.setSuccess_count(0);
			referenceInfo.setStatus(updateStatusList);
			referenceInfo.setStatus_message("error");
			referenceInfo.setEnd_timestamp(statusInfo.getEnd_timestamp());
		}else {
			if(statusInfo.getSeqNumber()==ingestionStepsArr.length) {
				for(StatusModel status:statusList) {
					StatusModel updateStatus=new StatusModel();
					if(status.getSeqNumber()==statusInfo.getSeqNumber()) {
						updateStatus.setStepName(statusInfo.getStepName());
						updateStatus.setStatus(statusInfo.getStatus());
						updateStatus.setSeqNumber(statusInfo.getSeqNumber());
						updateStatus.setEnd_timestamp(statusInfo.getEnd_timestamp());
						updateStatusList.add(updateStatus);
						}
					else {
						updateStatus.setStepName(status.getStepName());
						updateStatus.setStatus(status.getStatus());
						updateStatus.setSeqNumber(status.getSeqNumber());
						updateStatus.setEnd_timestamp(status.getEnd_timestamp());
						updateStatusList.add(updateStatus);
						}
				}
				referenceInfo.setStatus(updateStatusList);
				referenceInfo.setStatus_message("completed");
				referenceInfo.setEnd_timestamp(statusInfo.getEnd_timestamp());

				try {
					 Path path = Paths.get(baseDirectory+pathSeperator+statusInfo.getTenantName()+pathSeperator+referenceInfo.getIngestion_id()+pathSeperator+defaultInputFileName);
					 long lineCount;
					 try (Stream<String> stream = Files.lines(path)) {
					      lineCount = stream.count();
					 }
					 referenceInfo.setError_count(referenceInfo.getSuccess_count()-lineCount);
					 referenceInfo.setSuccess_count(lineCount);
					 
				}catch(Exception e) {
					referenceInfo.setSuccess_count(0);
					referenceInfo.setError_count(0);
				}
			}else {
				for(StatusModel status:statusList) {
					StatusModel updateStatus=new StatusModel();
					if(status.getSeqNumber()==statusInfo.getSeqNumber()) {
						updateStatus.setStepName(statusInfo.getStepName());
						updateStatus.setStatus(statusInfo.getStatus());
						updateStatus.setSeqNumber(statusInfo.getSeqNumber());
						updateStatus.setEnd_timestamp(statusInfo.getEnd_timestamp());
						updateStatusList.add(updateStatus);
						}
					else {
						updateStatus.setStepName(status.getStepName());
						updateStatus.setStatus(status.getStatus());
						updateStatus.setSeqNumber(status.getSeqNumber());
						updateStatus.setEnd_timestamp(status.getEnd_timestamp());
						updateStatusList.add(updateStatus);
						}
				}
				referenceInfo.setStatus(updateStatusList);
			}
			
		}
		
		businessModelIngestionService.saveReferenceInfo(referenceInfo,statusInfo.getTenantName());
	}
	
}
