package com.digitate.bizops.business.data.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitate.bizops.business.data.model.DatabaseServerInfo;
import com.digitate.bizops.business.data.model.TenantInfo;

@Service
public class GlobalDbServiceImpl {

	@Autowired
	GlobalDbService globalDbService;
	
	public List<TenantInfo> getTenantInfo(){
		return globalDbService.getAllTenants();
	}
	public DatabaseServerInfo getDataBaseInfoById(int id) {
		return globalDbService.getDatabaseServerInfoById(id);
	}
	public TenantInfo getTenantInfo(String tenant) {
		return globalDbService.getTenantInfo(tenant);
	}
	public TenantInfo getTenantInfoByService(String tenant,String service) {
		return globalDbService.getTenantInfoByService(tenant,service);
	}
}
