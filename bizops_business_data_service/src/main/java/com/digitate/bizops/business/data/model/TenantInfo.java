package com.digitate.bizops.business.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tenant_info")
public class TenantInfo {
	@SequenceGenerator(allocationSize=1, initialValue=1, sequenceName="tenant_info_id_seq", name="tenant_info_id_seq")
	@GeneratedValue(generator="tenant_info_id_seq", strategy=GenerationType.SEQUENCE)
	@Id
	private int id;
	@Column(name = "tenant_name")
	private String tenantName;
	@Column(name = "tenant_db_name")
	private String tenantDbName;
	@Column(name = "db_server_id")
	private int dbServerId;
	@Column(name = "service")
	private String service;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getTenantDbName() {
		return tenantDbName;
	}
	public void setTenantDbName(String tenantDbName) {
		this.tenantDbName = tenantDbName;
	}
	public int getDbServerId() {
		return dbServerId;
	}
	public void setDbServerId(int dbServerId) {
		this.dbServerId = dbServerId;
	}
	
	
}