package com.digitate.bizops.business.data;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.digitate.bizops.business.data.intereceptor.TenantNameExtractionFilter;



@SpringBootApplication
public class BizopsBusinessDataServiceApplication {
	@Value("${spring.datasource.username}")
    private String globalDbUser;
    @Value("${spring.datasource.password}")
    private String globalDbPassword;
    @Value("${spring.datasource.url}")
    private String globalDB;
    @Value("${spring.datasource.driver-class-name}")
    private String classDriverName;
    
	public static void main(String[] args) {
		SpringApplication.run(BizopsBusinessDataServiceApplication.class, args);
	}
	@Bean
    public FilterRegistrationBean < TenantNameExtractionFilter > filterRegistrationBean() {
        FilterRegistrationBean < TenantNameExtractionFilter > registrationBean = new FilterRegistrationBean();
        TenantNameExtractionFilter customURLFilter = new TenantNameExtractionFilter();
        registrationBean.setFilter(customURLFilter);
        registrationBean.addUrlPatterns("/enterprise_data/*");
        registrationBean.setOrder(2); //set precedence
        return registrationBean;
    }
	
	@Bean
	DataSource dataSource() {
		DriverManagerDataSource dataSourceConfig = new DriverManagerDataSource();
	    dataSourceConfig.setDriverClassName(classDriverName);

	    dataSourceConfig.setUrl(globalDB);
	    dataSourceConfig.setUsername(globalDbUser);
	    dataSourceConfig.setPassword(globalDbPassword);

	    return dataSourceConfig;
	}
}
