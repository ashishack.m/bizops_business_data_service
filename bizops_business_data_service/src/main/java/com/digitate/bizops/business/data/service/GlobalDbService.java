package com.digitate.bizops.business.data.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.digitate.bizops.business.data.model.DatabaseServerInfo;
import com.digitate.bizops.business.data.model.TenantInfo;
import com.digitate.bizops.business.data.repository.DatabaseServerInfoRepository;
import com.digitate.bizops.business.data.repository.TenantInfoRepository;

@Repository
@Transactional
public class GlobalDbService {

	@Autowired
	TenantInfoRepository tenantInfoRepository; 
	@Autowired
	DatabaseServerInfoRepository databaseServerInfoRepository;
	public List<TenantInfo> getAllTenants(){
		return tenantInfoRepository.findAll();
	}
	public DatabaseServerInfo getDatabaseServerInfoById(int id) {
		return databaseServerInfoRepository.findById(id);
	}
	public TenantInfo getTenantInfo(String tenant) {
		return tenantInfoRepository.findByTenantName(tenant);
	}
	public TenantInfo getTenantInfoByService(String tenant,String service) {
		return tenantInfoRepository.findByTenantNameAndService(tenant,service);
	}
}
