package com.digitate.bizops.business.data.model;


public class DatabaseConfiguration {
	private  String tenant;
    private  String host;
    private  String user;
    private  String dataSourceClassName;
    private  String password;
    private  String dbName;
    private int minIdleConnection;
    private int maxPoolSize;
    
    
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public int getMinIdleConnection() {
		return minIdleConnection;
	}
	public void setMinIdleConnection(int minIdleConnection) {
		this.minIdleConnection = minIdleConnection;
	}
	public int getMaxPoolSize() {
		return maxPoolSize;
	}
	public void setMaxPoolSize(int maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}
	public String getTenant() {
		return tenant;
	}
	public void setTenant(String tenant) {
		this.tenant = tenant;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getDataSourceClassName() {
		return dataSourceClassName;
	}
	public void setDataSourceClassName(String dataSourceClassName) {
		this.dataSourceClassName = dataSourceClassName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
    
}
