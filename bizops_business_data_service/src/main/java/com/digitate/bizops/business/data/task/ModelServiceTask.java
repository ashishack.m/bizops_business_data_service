package com.digitate.bizops.business.data.task;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.task.launcher.TaskLaunchRequest;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;



@Service
@EnableBinding(org.springframework.cloud.stream.messaging.Source.class)
public class ModelServiceTask {
	@Autowired
    private Source source;
	@Value("${spring.application.name}")
	private String applicationName;
	
	
	public void publishRequest(String configPath,String tempConfigPath,String dataPath,String dbMapPath,String referenceId,String tenantName)
	{
		String url = "maven://com.digitate.bizops.ingestion:bizops_business_data_ingestion_task:jar:0.0.1-SNAPSHOT";
		List<String> commandLineArguments = new ArrayList<String>();
		commandLineArguments.add(configPath);
		commandLineArguments.add(tempConfigPath);
		commandLineArguments.add(dataPath);
		commandLineArguments.add(dbMapPath);
		commandLineArguments.add(referenceId);
		commandLineArguments.add(tenantName);
		TaskLaunchRequest request = new TaskLaunchRequest(url, commandLineArguments, null, null, applicationName);

        GenericMessage < TaskLaunchRequest > message = new GenericMessage < > (request);
        // this call places the message on queue
        this.source.output().send(message);
		
	};
}
