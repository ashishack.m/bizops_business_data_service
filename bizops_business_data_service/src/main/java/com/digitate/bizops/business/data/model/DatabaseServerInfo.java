package com.digitate.bizops.business.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "database_server_info")
public class DatabaseServerInfo {
	@SequenceGenerator(allocationSize=1, initialValue=1, sequenceName="database_server_info_id_seq", name="database_server_info_id_seq")
	@GeneratedValue(generator="database_server_info_id_seq", strategy=GenerationType.SEQUENCE)
	@Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name = "type")
	private String type;
	@Column(name = "ip_address")
	private String ipAddress;
	@Column(name = "class_name")
	private String className;
	@Column(name = "user_name")
	private String userName;
	@Column(name = "password")
	private String password;
	@Column(name = "min_idle_conn")
	private int minIdleConnection;
	@Column(name = "max_pool_size")
    private int maxPoolSize;
    
    
	
	public int getMinIdleConnection() {
		return minIdleConnection;
	}
	public void setMinIdleConnection(int minIdleConnection) {
		this.minIdleConnection = minIdleConnection;
	}
	public int getMaxPoolSize() {
		return maxPoolSize;
	}
	public void setMaxPoolSize(int maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	
}
