package com.digitate.bizops.business.data.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.digitate.bizops.business.data.model.InProgressModel;
import com.digitate.bizops.business.data.model.IngestionModel;
import com.digitate.bizops.business.data.model.RequestHistoryUiModel;
import com.digitate.bizops.business.data.model.StatusModel;
import com.digitate.bizops.business.data.service.BusinessModelIngestionService;
import com.digitate.bizops.business.data.task.ModelServiceTask;
import com.digitate.bizops.business.data.thread.ThreadLocalStorage;

@RestController
@RequestMapping("/enterprise_data")
public class EnterpriseBusinessModelController {
	@Autowired
	BusinessModelIngestionService businessModelIngestionService;
	@Autowired
	ModelServiceTask modelServiceTask;
	
	@Value("${baseDir.path}")
	private String baseDirectory;
	@Value("${path.seperator}")
	private String pathSeperator;
	@Value("${default.input.file.name}")
	private String defaultInputFileName;
	@Value("${default.config.file.name}")
	private String defaultJsonFileName;
	@Value("${default.template.config.name}")
	private String defaultTemplateFileName;
	@Value("${default.dbMap.config.name}")
	private String defaultDbMapFileName;
	
	
	@RequestMapping(value="/ingest", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public HashMap<String, String> runAnalysis(@RequestParam("remark") String remark,@RequestParam("ingestion_id") String ingestionId,@RequestParam("entity_name") String modelName)
	{
		HashMap<String, String> result = new HashMap<String, String>();
		try {
				
				DecimalFormat decimalFormat = new DecimalFormat("00");
				long referenceId = businessModelIngestionService.generateSequence("business_run_analysis_seq");
				LocalDate currentdate = LocalDate.now();
				String referenceID = currentdate.getYear() +"-"+decimalFormat.format(currentdate.getMonthValue())+"-"+decimalFormat.format(currentdate.getDayOfMonth())+"-" + "BM"+ "-"+String.format("%05d", referenceId);
				result.put("reference_id", referenceID);
				Map<String,Object> dbSettingMap=new HashMap<String,Object>();
				//assumptions
				Date start_date = new Date();
			    
				dbSettingMap.put("exchange_rate", (double)1.0);
				dbSettingMap.put("spend_bins","-1,10,100,1000,5000,20000,40000,60000,1000000000000");
				dbSettingMap.put("spend_labels","LT 10,10 to 100,100 to 1000,1000 to 5000,5000 to 20000,20000 to 40000,40000 to 60000, GT 60000");
				dbSettingMap.put("base_currency","usd");
				dbSettingMap.put("user_id",ThreadLocalStorage.getUserName());
				dbSettingMap.put("date_format","MM-dd-yyyy");
				SimpleDateFormat ft =  new SimpleDateFormat ((String) dbSettingMap.get("date_format"));
				dbSettingMap.put("ingestion_date",ft.format(start_date));
				//end
				JSONObject json = new JSONObject();
			    json.putAll( dbSettingMap );
			    try (Writer out = new FileWriter(baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+ingestionId+pathSeperator+defaultDbMapFileName)) {
			        JSONObject.writeJSONString(json, out);
			    }
			    Path path = Paths.get(baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+ingestionId+pathSeperator+defaultInputFileName);
			    long lineCount;
			    try (Stream<String> stream = Files.lines(path,StandardCharsets.ISO_8859_1)) {
			      lineCount = stream.count();
			    }
			    
				businessModelIngestionService.CreateNewRequest(ThreadLocalStorage.getTenantName(), remark, referenceID,ingestionId,lineCount-1,referenceId);
				modelServiceTask.publishRequest(baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+ingestionId+pathSeperator+defaultJsonFileName,baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+ingestionId+pathSeperator+defaultTemplateFileName,baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+ingestionId+pathSeperator+defaultInputFileName,baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+ingestionId+pathSeperator+defaultDbMapFileName,referenceID,ThreadLocalStorage.getTenantName());
				
				return result ; 
		}catch(Exception e) {
			result=new HashMap<String, String>();
			result.put("status", "failed");
			return result;
			
		}
	}
	
	
	@RequestMapping(value="/save_config", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public HashMap<String, Object> saveConfig(@RequestParam("data_map") String dataMap,@RequestParam("ingestion_id") String ingestionId,@RequestParam("user_id") String userId,@RequestParam("entity_name") String modelName)
	{
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		FileWriter file=null;
        
		try {
			file = new FileWriter(baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+ingestionId+pathSeperator+defaultJsonFileName);
			businessModelIngestionService.saveRecommendation(userId,dataMap,modelName);
			file.write(dataMap);
			file.flush();
			file.close();
			result.put("ingestion_id", ingestionId);
			result.put("status", "success");
	 
	        } catch(Exception e) {
	        	result=new HashMap<String, Object>();
	        	result.put("ingestion_id", ingestionId);
				result.put("status", "failure");
			}
		return result ; 
	}
	
	@RequestMapping(value="/status", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public List<StatusModel> getStatus(@RequestParam("reference_id") String referenceId)
	{
		
		List<StatusModel> result= new ArrayList<StatusModel>();
        
		try {
			result=businessModelIngestionService.getStatus(referenceId);
			
	        } catch(Exception e) {
	        	result=new ArrayList<StatusModel>();
			}
		return result ; 
	}
	
	@RequestMapping(value="/in_progress", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public List<InProgressModel> getStatus()
	{
		
		List<InProgressModel> result= null;
        
		try {
			result=businessModelIngestionService.getInProgressInfo(ThreadLocalStorage.getTenantName(), 5);
			
	        } catch(Exception e) {
	        	result=new ArrayList<InProgressModel>();
			}
		return result ; 
	}
	
	@RequestMapping(value="/ingestion_history", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public HashMap<String, Object> getIngestionHistory(@RequestParam("user_id") String user,@RequestParam("page_no") int pageNo ,@RequestParam("page_size") int pageSize)
	{
		HashMap<String, Object> historyResult = null;
		try {
			historyResult=businessModelIngestionService.getHistory(user,pageNo,pageSize);
			
		} catch(Exception e) {
			historyResult = new HashMap<String, Object>();
		}
		return historyResult ; 
	}
	
	@RequestMapping(value="/upload", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public HashMap<String, Object> upload(@RequestParam("file") MultipartFile file,@RequestParam("entity_name") String modelName)
	{
		HashMap<String, Object> result = new HashMap<String, Object>();
		DecimalFormat decimalFormat = new DecimalFormat("00");
		long referenceId = businessModelIngestionService.generateSequence("business_ingestion_seq");
		LocalDate currentdate = LocalDate.now();
		String referenceID="";
		
		try {
			referenceID = currentdate.getYear() +"-"+decimalFormat.format(currentdate.getMonthValue())+"-"+decimalFormat.format(currentdate.getDayOfMonth())+"-" + "BI"+ "-"+String.format("%05d", referenceId);
		
			Path path = Paths.get(baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+referenceID);
			Files.createDirectories(path);
			
			file.transferTo(new File(baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+referenceID+pathSeperator+defaultInputFileName));
			
			IngestionModel model=businessModelIngestionService.getModel(modelName);
			//JSONObject json=new JSONObject(model.getEntity_info());
			//templateFile = new FileWriter(baseDirectory+pathSeperator+referenceID+pathSeperator+defaultTemplateFileName);
			try (Writer out = new FileWriter(baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+referenceID+pathSeperator+defaultTemplateFileName)) {
		        JSONObject.writeJSONString(model.getEntity_info(), out);
		    }
			//templateFile.write(model.getEntity_info().toString());
			
			result.put("ingested_data_headers", businessModelIngestionService.getDataHeaders(baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+referenceID+pathSeperator+defaultInputFileName));
			result.put("recommendation_mapping",businessModelIngestionService.getRecommendation(ThreadLocalStorage.getUserName(),(String[]) result.get("ingested_data_headers"),modelName));
			result.put("drop_down_options", businessModelIngestionService.getModelMapList(modelName,"csv_template"));
			result.put("mandatory_mapping",businessModelIngestionService.getModelMapList(modelName,"csv_mandatory_template"));
			result.put("ingestion_id", referenceID);
			result.put("status", "success");
		}catch(Exception e) {
			try {
				Path path = Paths.get(baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+referenceID);
				Files.delete(path);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}finally {
			result=new HashMap<String, Object>();
			result.put("ingestion_id", null);
			result.put("status", "failed");
			}
		}
		
		return result ; 
	}
	
	@RequestMapping(value="/update_default", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public HashMap<String, Object> updateDefaultMapping(@RequestParam("entity_name") String modelName,@RequestParam("data_map") String dataMap)
	{
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		try {
			
			businessModelIngestionService.saveRecommendation("default",dataMap,modelName);
			
			result.put("status", "success");
		}catch(Exception e) {
			
			result.put("status", "failed");
			
		}
		
		return result ; 
	}
	
	@RequestMapping(value="/download_template",  method = RequestMethod.GET)
    public void downloadtemplate(HttpServletResponse response,@RequestParam("entity_name") String entity) 
    {
		response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=inputtemplate.zip");
        response.setStatus(HttpServletResponse.SC_OK);

        String downloadFileName = baseDirectory+pathSeperator+"template/"+entity+"_template.csv";
        try (ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream())) {
            
                FileSystemResource resource = new FileSystemResource(downloadFileName);

                ZipEntry e = new ZipEntry(resource.getFilename());
              
                e.setSize(resource.contentLength());
                e.setTime(System.currentTimeMillis());
               
                zippedOut.putNextEntry(e);
               
                StreamUtils.copy(resource.getInputStream(), zippedOut);
                zippedOut.closeEntry();
            
            zippedOut.finish();
        } catch (Exception e) {
        	 e.printStackTrace();
        }
    }
	@RequestMapping(value="/download_report",  method = RequestMethod.GET)
    public void downloadDiagnostic(HttpServletResponse response,@RequestParam("ingestion_id") String ingestionId,@RequestParam("user_id") String user) 
    {
		response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=Diagnostic_report.zip");
        response.setStatus(HttpServletResponse.SC_OK);

        String[] fileNames ={baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+ingestionId+pathSeperator+"input_error.csv",baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+ingestionId+pathSeperator+"input_diagnostic.txt",baseDirectory+pathSeperator+ThreadLocalStorage.getTenantName()+pathSeperator+ingestionId+pathSeperator+"input.csv"};
        try (ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream())) {
        	 for(String file : fileNames) {
                FileSystemResource resource = new FileSystemResource(file);

                ZipEntry e = new ZipEntry(resource.getFilename());
              
                e.setSize(resource.contentLength());
                e.setTime(System.currentTimeMillis());
               
                zippedOut.putNextEntry(e);
               
                StreamUtils.copy(resource.getInputStream(), zippedOut);
        	 }   
        	 zippedOut.closeEntry();
            zippedOut.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

